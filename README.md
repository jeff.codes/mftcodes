A Marriage and Family Therapist turned coder. I love what I do. I value privacy, security, FOSS, and giving back. With luck, I'll be able to apply my skills to those ends.

Can also be found @ => <a rel="me" href="https://fosstodon.org/@JeffCodes">Mastodon</a> occassionally "tooting" along the way. 
